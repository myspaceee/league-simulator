<?php

use App\Services\League\Facades\League;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    $uuid = League::createAndSave();

    League::playWeek($uuid);

    return dd(League::getLeagueResults($uuid));
});

Route::view('/', 'welcome');
Route::view('/league/{leagueUUID}', 'league')->name('league.show');
