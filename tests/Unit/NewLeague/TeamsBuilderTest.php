<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\TeamsBuilder;
use App\Services\League\Exceptions\NotEnoughTeamsException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\TestCase;

class TeamsBuilderTest extends TestCase
{
    /**
     * @var Storage|mixed|\PHPUnit\Framework\MockObject\MockObject
     */
    private mixed $storage;

    public function getFileContent()
    {
        return json_encode([
            [
                'name' => 'team 1'
            ],
            [
                'name' => 'team 2'
            ],
            [
                'name' => 'team 3'
            ],
            [
                'name' => 'team 4'
            ],
            [
                'name' => 'team 5'
            ],
            [
                'name' => 'team 6'
            ],
            [
                'name' => 'team 7'
            ],
        ]);
    }

    protected function setUp(): void
    {
        $this->storage = $this->createMock(Filesystem::class);

        $this->storage->method('get')->willReturn(
            $this->getFileContent()
        );
    }

    public function testBuild()
    {
        $teamsBuilder = new TeamsBuilder($this->storage);

        $teams = $teamsBuilder->build(5);

        $this->assertIsArray($teams);

        $this->assertCount(5, $teams);
    }

    public function testThatNotEnoughTeamsInFile()
    {
        $this->expectException(NotEnoughTeamsException::class);

        $teamsBuilder = new TeamsBuilder($this->storage);

        $teamsBuilder->build(500);
    }
}
