<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\Game;
use App\Services\League\Classes\Team;
use App\Services\League\Strategies\PlayNextAllStrategy;
use PHPUnit\Framework\TestCase;

class PlayNextAllStrategyTest extends TestCase
{
    public function testThatResultsAreWithCorrectStructure()
    {
        $match = $this->createMock(Game::class);

        $team = $this->createMock(Team::class);

        $team->method('getUuid')->willReturnOnConsecutiveCalls(
            '123', '234'
        );

        $match->method('getTeams')->willReturn([
            $team, $team
        ]);
        $match->method('getMappedGoals')->willReturn([
            '123' => 2,
            '234' => 3
        ]);

        $playNextAllStrategy = new PlayNextAllStrategy();

        $results = $playNextAllStrategy->play(
            1,
            0,
            [$match]
        );

        $this->assertArrayHasKey('week', $results);
        $this->assertArrayHasKey('matches', $results);
        $this->assertCount(1, $results['matches']);
    }

    public function testThatCurrentWeekIsLastAfterPlayWhenStarted()
    {
        $match = $this->createMock(Game::class);
        $team = $this->createMock(Team::class);

        $match->method('getTeams')->willReturn([$team, $team]);
        $match->method('getMappedGoals')->willReturnOnConsecutiveCalls(
            ['123' => 2, '234' => 3],
            ['12345' => 2, '12346' => 3]
        );
        $team->method('getUuid')->willReturnOnConsecutiveCalls(
            '123', '234', '12345', '12346'
        );

        $playNextAllStrategy = new PlayNextAllStrategy();

        $results = $playNextAllStrategy->play(
            1,
            0,
            [$match, $match]
        );

        $this->assertEquals((count([$match, $match]) / 1), $results['week']);
    }

    public function testThatCurrentWeekIsLastAfterPlayWhenAnyAllowed()
    {
        $match = $this->createMock(Game::class);
        $team = $this->createMock(Team::class);

        $match->method('getTeams')->willReturn([$team, $team]);
        $match->method('getMappedGoals')->willReturnOnConsecutiveCalls(
            ['123' => 2, '234' => 3],
            ['12345' => 2, '12346' => 3]
        );
        $team->method('getUuid')->willReturnOnConsecutiveCalls(
            '123', '234', '12345', '12346'
        );

        $playNextAllStrategy = new PlayNextAllStrategy();

        $results = $playNextAllStrategy->play(
            1,
            1,
            [$match, $match]
        );

        $this->assertEquals((count([$match, $match]) / 1), $results['week']);
    }
}
