<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\League;
use App\Services\League\Events\LeaguePlayedEvent;
use PHPUnit\Framework\TestCase;

class LeaguePlayedEventTest extends TestCase
{
    public function testThatTeamsAreSet()
    {
        $league = $this->createMock(League::class);

        $league->method('getTeams')->willReturn([]);

        $leaguePlayedEvent = new LeaguePlayedEvent(
            $league
        );

        $this->assertEquals([], $leaguePlayedEvent->teams);
    }
}
