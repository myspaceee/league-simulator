<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\Game;
use App\Services\League\Classes\Team;
use App\Services\League\Strategies\PlayNextAllStrategy;
use App\Services\League\Strategies\PlayWeekStrategy;
use PHPUnit\Framework\TestCase;

class PlayWeekStrategyTest extends TestCase
{
    public function testThatResultsAreWithCorrectStructure()
    {
        $match = $this->createMock(Game::class);

        $team = $this->createMock(Team::class);

        $team->method('getUuid')->willReturnOnConsecutiveCalls(
            '123', '234', '321', '4321'
        );

        $match->method('getTeams')->willReturn([
            $team, $team
        ]);
        $match->method('getMappedGoals')->willReturnOnConsecutiveCalls(
            ['123' => 2, '234' => 3],
            ['321' => 2, '4321' => 3],
        );

        $playWeekStrategy = new PlayWeekStrategy();

        $per_week = 2;

        $results = $playWeekStrategy->play(
            $per_week,
            0,
            [$match, $match]
        );

        $this->assertArrayHasKey('week', $results);
        $this->assertArrayHasKey('matches', $results);
        $this->assertCount($per_week, $results['matches']);
    }


    public function testThatCurrentWeekIsLastAfterPlayWhenStarted()
    {
        $match = $this->createMock(Game::class);
        $team = $this->createMock(Team::class);

        $match->method('getTeams')->willReturn([$team, $team]);
        $match->method('getMappedGoals')->willReturnOnConsecutiveCalls(
            ['123' => 2, '234' => 3],
            ['12345' => 2, '12346' => 3]
        );
        $team->method('getUuid')->willReturnOnConsecutiveCalls(
            '123', '234', '12345', '12346'
        );

        $playWeekStrategy = new PlayWeekStrategy();

        $current_week = 0;

        $results = $playWeekStrategy->play(
            1,
            $current_week,
            [$match, $match]
        );

        $this->assertEquals($current_week + 1, $results['week']);
    }

    public function testThatCurrentWeekIsLastAfterPlayWhenAnyAllowed()
    {
        $match = $this->createMock(Game::class);
        $team = $this->createMock(Team::class);

        $match->method('getTeams')->willReturn([$team, $team]);
        $match->method('getMappedGoals')->willReturnOnConsecutiveCalls(
            ['123' => 2, '234' => 3],
            ['12345' => 2, '12346' => 3]
        );
        $team->method('getUuid')->willReturnOnConsecutiveCalls(
            '123', '234', '12345', '12346'
        );

        $playWeekStrategy = new PlayWeekStrategy();

        $results = $playWeekStrategy->play(
            1,
            1,
            [$match, $match]
        );

        $this->assertEquals(2, $results['week']);
    }
}
