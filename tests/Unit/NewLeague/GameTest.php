<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\CalculateGoals;
use App\Services\League\Classes\Game;
use App\Services\League\Classes\Team;
use App\Services\League\Exceptions\GameMembersException;
use App\Services\League\Factories\CalculateGoalsFactory;
use App\Services\League\Factories\GameTeamResultsFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    private MockObject|CalculateGoalsFactory $calculateGoalsFactoryMock;

    private MockObject|GameTeamResultsFactory $gameTeamResultsFactory;

    public function wrongTeamsProvider()
    {
        return [
            [[1]],
            [[1, 2, 3, 4]],
            [[1, 2, 3]],
            [[]],
        ];
    }

    public function teamsUuidsProvider()
    {
        return [
            ['123', '234']
        ];
    }

    protected function setUp(): void
    {
        $this->calculateGoalsFactoryMock = $this
            ->getMockBuilder(CalculateGoalsFactory::class)
            ->getMock();

        $this->gameTeamResultsFactory = $this
            ->getMockBuilder(GameTeamResultsFactory::class)
            ->getMock();
    }

    /**
     * @dataProvider wrongTeamsProvider
     */
    public function testThatTwoTeamsRequiredCondition($teams_with_wrong_number)
    {
        $this->expectException(GameMembersException::class);

        new Game(
            $teams_with_wrong_number,
            $this->calculateGoalsFactoryMock,
            $this->gameTeamResultsFactory
        );
    }

    /**
     * @param $team1_uuid
     * @param $team2_uuid
     * @throws GameMembersException
     * @dataProvider teamsUuidsProvider
     */
    public function testThatWeCreateTeamResultsForTwoTeams($team1_uuid, $team2_uuid)
    {
        $team_1 = $this->getMockBuilder(Team::class)
            ->setConstructorArgs(['Team 1'])->getMock();
        $team_1->method('getUuid')->willReturn($team1_uuid);

        $team_2 = $this->getMockBuilder(Team::class)
            ->setConstructorArgs(['Team 2'])->getMock();
        $team_2->method('getUuid')->willReturn($team2_uuid);

        $calculateGoalsMock = $this
            ->getMockBuilder(CalculateGoals::class)
            ->disableOriginalConstructor()
            ->getMock();

        $calculateGoalsMock->method('calculate')->willReturn([
            2, 3
        ]);

        $this->calculateGoalsFactoryMock->method('build')->willReturn(
            $calculateGoalsMock
        );

        $game = new Game(
            [$team_1, $team_2],
            $this->calculateGoalsFactoryMock,
            $this->gameTeamResultsFactory
        );

        $mappedGoals = [$team1_uuid => 2, $team2_uuid => 3];

        $this->gameTeamResultsFactory
            ->expects($this->exactly(2))
            ->method('build')
            ->with(
                $this->equalTo($mappedGoals),
                $this->callback(function ($uuid) use ($team1_uuid, $team2_uuid) {
                    return in_array($uuid, [$team1_uuid, $team2_uuid]);
                })
            );

        $team_1->expects($this->once())->method('addGameResults');
        $team_2->expects($this->once())->method('addGameResults');

        $game->play();
    }


    public function testThatGameGetTeamsResultIsArray()
    {
        $team = $this->createMock(Team::class);

        $game = new Game(
            [$team, $team],
            $this->calculateGoalsFactoryMock,
            $this->gameTeamResultsFactory
        );

        $this->assertIsArray($game->getTeams());
    }

    public function testThatGameGetMappedGoalsResultIsArray()
    {
        $team = $this->createMock(Team::class);

        $game = new Game(
            [$team, $team],
            $this->calculateGoalsFactoryMock,
            $this->gameTeamResultsFactory
        );

        $this->assertIsArray($game->getMappedGoals());
    }
}
