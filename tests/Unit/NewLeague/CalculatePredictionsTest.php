<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\League;
use App\Services\League\Classes\Team;
use App\Services\League\Events\LeaguePlayedEvent;
use App\Services\League\Listeners\CalculatePredictions;
use PHPUnit\Framework\TestCase;

class CalculatePredictionsTest extends TestCase
{
    private CalculatePredictions $listener;
    /**
     * @var League|mixed|\PHPUnit\Framework\MockObject\MockObject
     */
    private League $league;

    protected function setUp(): void
    {
        $this->listener = new CalculatePredictions();
        $this->league = $this->createMock(League::class);
    }

    public function testThatExpectedPredictionsAreCorrect()
    {
        $team_1 = $this->createStub(Team::class);
        $team_2 = $this->createStub(Team::class);

        $team_1->method('getPts')->willReturn(25);
        $team_1->method('getPlayed')->willReturn(1);

        $team_2->method('getPts')->willReturn(0);
        $team_2->method('getPlayed')->willReturn(0);

        $teams = [$team_1, $team_2];

        $this->league->method('getTeams')->willReturn($teams);

        $teams[0]->expects($this->once())
            ->method('setPrediction')
            ->with($this->equalTo(CalculatePredictions::MAX_PREDICTION));

        $teams[1]->expects($this->never())->method('setPrediction');

        $this->listener->handle($this->league);
    }

    public function testThatMaxPredictionForTeamIsConst()
    {
        $team_1 = $this->createStub(Team::class);
        $team_2 = $this->createStub(Team::class);

        $team_1->method('getPts')->willReturn(40);
        $team_1->method('getPlayed')->willReturn(1);

        $team_2->method('getPts')->willReturn(100);
        $team_2->method('getPlayed')->willReturn(1);

        $teams = [$team_1, $team_2];

        $this->league->method('getTeams')->willReturn($teams);

        $teams[0]->expects($this->once())
            ->method('setPrediction')
            ->with($this->equalTo(29));

        $teams[1]->expects($this->once())
            ->method('setPrediction')
            ->with($this->equalTo(71));

        $this->listener->handle($this->league);
    }
}
