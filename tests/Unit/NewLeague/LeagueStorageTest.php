<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\League;
use App\Services\League\Classes\LeagueStorage;
use App\Services\League\Factories\LeagueFactoryRestore;
use Illuminate\Contracts\Cache\Repository;
use PHPUnit\Framework\TestCase;

class LeagueStorageTest extends TestCase
{
    protected function setUp(): void
    {

    }

    public function provider()
    {
        return [
            ['123', 1, [], [], 2, []]
        ];
    }

    /**
     * @dataProvider provider
     */
    public function testThatStoragePutCallWithCorrectParams($uuid, $week, $teams, $matches, $per_week, $last_played_matches)
    {
        $storage = $this->createMock(Repository::class);

        $leagueStorage = new LeagueStorage(
            $storage,
            $this->createMock(LeagueFactoryRestore::class)
        );

        $league = $this->createMock(League::class);

        $league->method('getUuid')->willReturn($uuid);
        $league->method('getCurrentWeek')->willReturn($week);
        $league->method('getTeams')->willReturn($teams);
        $league->method('getMatches')->willReturn($matches);
        $league->method('getMatchesPerWeek')->willReturn($per_week);
        $league->method('getLastPlayedMatches')->willReturn($last_played_matches);

        $storage->expects($this->once())->method('put')
            ->with(
                $this->equalTo('league-' . $uuid),
                $this->equalTo([
                    'week' => $week,
                    'teams' => $teams,
                    'matches' => $matches,
                    'per_week' => $per_week,
                    'last_played_matches' => $last_played_matches
                ])
            );

        $leagueStorage->save($league);
    }

    /**
     * @dataProvider provider
     */
    public function testThatLeagueCreatingIsCorrect($uuid, $week, $teams, $matches, $per_week, $last_played_matches)
    {
        $storage = $this->createMock(Repository::class);

        $restoreFactory = $this->createMock(LeagueFactoryRestore::class);

        $leagueStorage = new LeagueStorage(
            $storage,
            $restoreFactory
        );

        $storage->expects($this->once())->method('get')
            ->with($this->equalTo('league-' . $uuid))
            ->willReturn([
                'week' => $week,
                'teams' => $teams,
                'matches' => $matches,
                'per_week' => $per_week,
                'last_played_matches' => $last_played_matches
            ]);

        $restoreFactory->expects($this->once())
            ->method('restore')
            ->with(
                $this->equalTo($uuid),
                $this->equalTo($per_week),
                $this->equalTo($teams),
                $this->equalTo($matches),
                $this->equalTo($week),
                $this->equalTo($last_played_matches),
            );

        $leagueStorage->get($uuid);
    }
}
