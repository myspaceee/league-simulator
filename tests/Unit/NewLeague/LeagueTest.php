<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\League;
use App\Services\League\Classes\PlayStrategyResolver;
use App\Services\League\Contracts\PlayStrategyContract;
use App\Services\League\Events\LeaguePlayedEvent;
use App\Services\League\Exceptions\LeagueAlreadyFinishedException;
use App\Services\League\Exceptions\MatchesNumberException;
use App\Services\League\Factories\MatchesPlannerFactory;
use Illuminate\Contracts\Events\Dispatcher;
use PHPUnit\Framework\TestCase;

class LeagueTest extends TestCase
{
    private PlayStrategyResolver $playStrategyResolver;
    private MatchesPlannerFactory $matchesPlannerFactory;
    private Dispatcher $dispatcher;

    private PlayStrategyContract $playStrategyContract;

    protected function setUp(): void
    {
        $this->playStrategyResolver = $this->createMock(PlayStrategyResolver::class);
        $this->matchesPlannerFactory = $this->createMock(MatchesPlannerFactory::class);
        $this->dispatcher = $this->createMock(Dispatcher::class);
        $this->playStrategyContract = $this->createMock(PlayStrategyContract::class);
    }

    private function getWorkedLeague()
    {
        return new League(
            uniqid(),
            [[], [], [], []],
            $this->playStrategyResolver,
            $this->matchesPlannerFactory,
            $this->dispatcher,
            2,
            [[], [], [], []]
        );
    }

    public function timesProvider()
    {
        return [
            ['week'],
            ['all']
        ];
    }

    public function testThatMatcherPerWeekIsLessThanMaxAllowed()
    {
        $this->expectException(MatchesNumberException::class);

        new League(
            uniqid(),
            [[], [], [], []],
            $this->playStrategyResolver,
            $this->matchesPlannerFactory,
            $this->dispatcher,
            20
        );
    }

    public function testThatUuidIsString()
    {
        $league = $this->getWorkedLeague();

        $this->assertIsString($league->getUuid());
    }

    public function testThatTeamsNumberIsTheSame()
    {
        $league = $this->getWorkedLeague();

        $teams = $league->getTeams();

        $this->assertIsArray($teams);
        $this->assertCount(4, $teams);
    }

    /**
     * @dataProvider timesProvider
     */
    public function testThatResolverGetCorrectTimeAndDispatched($time)
    {
        $league = $this->getWorkedLeague();

        $this->playStrategyContract->method('play')
            ->willReturn([
                'matches' => [],
                'week' => 1
            ]);

        $this->playStrategyResolver
            ->method('resolve')
            ->with($this->equalTo($time))
            ->willReturn($this->playStrategyContract);

        $this->dispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with(LeaguePlayedEvent::class, $league);

        $league->play($time);

        $this->assertEquals(1, $league->getCurrentWeek());
    }

    public function testThatWeReceiveExceptionWhenWantToPlayFinishedLeague()
    {
        $league = new League(
            uniqid(),
            [[], [], [], []],
            $this->playStrategyResolver,
            $this->matchesPlannerFactory,
            $this->dispatcher,
            2,
            array_fill(0, 12, []),
            current_week: 6
        );

        $this->expectException(LeagueAlreadyFinishedException::class);

        $league->play();

    }

    public function testThatLeagueTeamsIsArray()
    {
        $league = $this->getWorkedLeague();

        $this->assertIsArray($league->getMatches());
    }

    public function testThatGetMatchesPerWeekIsInteger()
    {
        $league = $this->getWorkedLeague();

        $this->assertIsInt($league->getMatchesPerWeek());
    }
}
