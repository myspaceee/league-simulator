<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\CalculateGoals;
use App\Services\League\Classes\Team;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CalculateGoalsTest extends TestCase
{
    private MockObject $team1;

    private MockObject $team2;

    private MockObject $goalsService;

    protected function setUp(): void
    {
        $this->team1 = $this->createMock(Team::class);

        $this->team2 = $this->createMock(Team::class);

        $this->goalsService = $this->getMockBuilder(CalculateGoals::class)
            ->setConstructorArgs([[$this->team1, $this->team2]])
            ->onlyMethods(['calculatePoints'])
            ->getMock();
    }

    public function testThatInMatchAreTwoTeams()
    {
        $calcGoals = new CalculateGoals([$this->team1, $this->team2]);

        $this->assertCount(2, $calcGoals->calculate());
    }

    public function testThatMatchHasHigherDrawnResult()
    {
        $this->goalsService->method('calculatePoints')->willReturn(true);

        $goals = $this->goalsService->calculate();

        $this->assertEquals([3, 3], $goals);

    }

    public function testThatMatchHasLowerDrawnResult()
    {
        $this->goalsService->method('calculatePoints')->willReturn(false);

        $goals = $this->goalsService->calculate();

        $this->assertEquals([2, 2], $goals);

    }

    public function testThatFirstTeamWon()
    {
        $this->goalsService->method('calculatePoints')->will($this->onConsecutiveCalls(true, false));

        $goals = $this->goalsService->calculate();

        $this->assertEquals([3, 2], $goals);

    }

    public function testThatSecondTeamWon()
    {
        $this->goalsService->method('calculatePoints')->will($this->onConsecutiveCalls(false, true));

        $goals = $this->goalsService->calculate();

        $this->assertEquals([2, 3], $goals);

    }
}
