<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\GameTeamResults;
use App\Services\League\Classes\Team;

use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase
{
    private Team $team;

    protected function setUp(): void
    {
        $this->team = new Team('Manchester');
    }

    public function testThatPredictionIsCorrect()
    {
        $this->team->setPrediction(12);

        $this->assertEquals(12, $this->team->getPrediction());
    }

    public function testThatNameIsCorrect()
    {
        $this->assertEquals('Manchester', $this->team->getName());
    }

    public function testThatUuidIsString()
    {
        $this->assertIsString($this->team->getUuid());
    }

    public function testThatPtsIsInteger()
    {
        $this->assertIsInt($this->team->getPTS());
    }

    public function testThatGDIsInteger()
    {
        $this->assertIsInt($this->team->getGd());
    }

    public function testNewGameWon()
    {
        $played = $this->team->getPlayed();

        $gameResults = $this->createMock(GameTeamResults::class);
        $gameResults->method('getGd')->willReturn(1);
        $gameResults->method('getPts')->willReturn(9);

        $this->team->addGameResults($gameResults);

        $this->assertNotEquals($played, $this->team->getPlayed());
        $this->assertEquals(1, $this->team->getGd());
        $this->assertEquals(9, $this->team->getPTS());
        $this->assertEquals(1, $this->team->getWon());
    }

    public function testNewFailGame()
    {
        $gameResults = $this->createMock(GameTeamResults::class);
        $gameResults->method('getGd')->willReturn(-1);
        $gameResults->method('getPts')->willReturn(9);

        $this->team->addGameResults($gameResults);

        $this->assertEquals(-1, $this->team->getGd());
        $this->assertEquals(1, $this->team->getLost());

    }

    public function testNewDrawnGame()
    {
        $gameResults = $this->createMock(GameTeamResults::class);
        $gameResults->method('getGd')->willReturn(0);
        $gameResults->method('getPts')->willReturn(9);

        $this->team->addGameResults($gameResults);

        $this->assertEquals(0, $this->team->getGd());
        $this->assertEquals(1, $this->team->getDrawn());
    }
}
