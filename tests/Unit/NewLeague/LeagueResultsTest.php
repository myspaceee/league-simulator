<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\League;
use App\Services\League\Responses\LeagueResults;
use App\Services\League\Classes\Team;
use PHPUnit\Framework\TestCase;

class LeagueResultsTest extends TestCase
{
    public function testThatResultFormatIsCorrect()
    {
        $league = $this->createMock(League::class);

        $team = $this->createMock(Team::class);

        $leagueResults = new LeagueResults($league);

        $league->method('getCurrentWeek')->willReturn(1);
        $league->method('getTeams')->willReturn([$team]);

        $results = $leagueResults->format();

        $this->assertArrayHasKey('current_week', $results);
        $this->assertArrayHasKey('teams', $results);
        $this->assertArrayHasKey('last_played_matches', $results);

        $this->assertEquals(1, $results['current_week']);

        $this->assertArrayHasKey('name', $results['teams'][0]);
        $this->assertArrayHasKey('pts', $results['teams'][0]);
        $this->assertArrayHasKey('played', $results['teams'][0]);
        $this->assertArrayHasKey('won', $results['teams'][0]);
        $this->assertArrayHasKey('drawn', $results['teams'][0]);
        $this->assertArrayHasKey('lost', $results['teams'][0]);
        $this->assertArrayHasKey('gd', $results['teams'][0]);
        $this->assertArrayHasKey('prediction_score', $results['teams'][0]);

    }
}
