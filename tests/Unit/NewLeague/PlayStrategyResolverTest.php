<?php

namespace Tests\Unit\NewLeague;

use App\Services\League\Classes\PlayStrategyResolver;
use App\Services\League\Contracts\PlayStrategyContract;
use PHPUnit\Framework\TestCase;

class PlayStrategyResolverTest extends TestCase
{
    public function provider()
    {
        return [
            ['week'],
            ['all'],
        ];
    }

    /**
     * @dataProvider provider
     */
    public function testThatReturnCorrectObject($type)
    {
        $playStrategyResolver = new PlayStrategyResolver();

        $result = $playStrategyResolver
            ->resolve($type);

        $this->assertInstanceOf(PlayStrategyContract::class, $result);

    }
}
