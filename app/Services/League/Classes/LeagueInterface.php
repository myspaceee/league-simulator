<?php

namespace App\Services\League\Classes;

use App\Services\League\Contracts\LeagueContract;
use App\Services\League\Factories\LeagueFactory;
use App\Services\League\Factories\LeagueResultsFactory;
use Illuminate\Contracts\Events\Dispatcher;
use JetBrains\PhpStorm\ArrayShape;

class LeagueInterface implements LeagueContract
{
    public function __construct(
        public Dispatcher                   $dispatcher,
        private LeagueStorage               $leagueStorage,
        private LeagueFactory               $leagueFactory,
        private LeagueResultsFactory        $leagueResultsFactory
    )
    {
    }

    /**
     * @throws \App\Services\League\Exceptions\MatchesNumberException
     */
    public function createAndSave(int $matches_per_week = 2, int $teams_number = 4): string
    {
        $league = $this->leagueFactory->build($matches_per_week, $teams_number);

        $this->leagueStorage->save($league);

        return $league->getUuid();
    }

    public function playWeek(string $league_uuid)
    {
        $league = $this->leagueStorage->get($league_uuid);

        $league->play();

        $this->leagueStorage->save($league);
    }

    public function playAllWeeks(string $league_uuid)
    {
        $league = $this->leagueStorage->get($league_uuid);

        $league->play('all');

        $this->leagueStorage->save($league);
    }

    #[ArrayShape(['current_week' => "int", 'teams' => "array"])] public function getLeagueResults(string $league_uuid): array
    {
        $league = $this->leagueStorage->get($league_uuid);

        return $this->leagueResultsFactory->build($league)->format();
    }
}
