<?php

namespace App\Services\League\Classes;

use App\Services\League\Contracts\PlayStrategyContract;
use App\Services\League\Strategies\PlayNextAllStrategy;
use App\Services\League\Strategies\PlayWeekStrategy;

class PlayStrategyResolver
{
    public function resolve(string $type): PlayStrategyContract
    {
        return match ($type){
            'week' => new PlayWeekStrategy(),
            'all' => new PlayNextAllStrategy()
        };
    }
}
