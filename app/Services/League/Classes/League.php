<?php

namespace App\Services\League\Classes;

use App\Services\League\Events\LeaguePlayedEvent;
use App\Services\League\Exceptions\LeagueAlreadyFinishedException;
use App\Services\League\Exceptions\MatchesNumberException;
use App\Services\League\Factories\MatchesPlannerFactory;
use Illuminate\Contracts\Events\Dispatcher;

class League
{
    public function __construct(
        private string                $uuid,
        private array                 $teams,
        private PlayStrategyResolver  $playStrategyResolver,
        private MatchesPlannerFactory $matchesPlannerFactory,
        private Dispatcher            $dispatcher,
        private int                   $matches_per_week,
        private array                 $matches = [],
        private int                   $current_week = 0,
        private array                 $last_played_matches = []
    )
    {
        $this->checkCanCreateLeague();

        if (!$matches) {
            $this->matches = $this->matchesPlannerFactory->build($this->teams);
        }
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getTeams(): array
    {
        return $this->teams;
    }

    public function getCurrentWeek(): int
    {
        return $this->current_week;
    }

    public function getMatches(): array
    {
        return $this->matches;
    }

    public function getMatchesPerWeek(): int
    {
        return $this->matches_per_week;
    }

    public function getLastPlayedMatches(): array
    {
        return $this->last_played_matches;
    }

    public function play(string $type = 'week')
    {
        if ($this->current_week === count($this->matches) / $this->matches_per_week) {
            throw new LeagueAlreadyFinishedException('This League already finished');
        }

        $playStrategy = $this->playStrategyResolver->resolve($type);

        [
            'matches' => $this->last_played_matches,
            'week' => $this->current_week
        ] = $playStrategy->play($this->matches_per_week, $this->current_week, $this->matches);

        $this->dispatcher->dispatch(LeaguePlayedEvent::class, $this);
    }

    /**
     * @throws MatchesNumberException
     */
    private function checkCanCreateLeague()
    {
        $matches_number = $this->calculateMatchesNumber();

        if ($this->matches_per_week > $matches_number) {
            throw new MatchesNumberException("Max value for matches per week is {$matches_number}");
        }
    }

    private function calculateMatchesNumber(): int
    {
        return (count($this->teams) - 1) * count($this->teams);
    }
}
