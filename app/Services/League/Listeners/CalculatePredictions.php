<?php

namespace App\Services\League\Listeners;

use App\Services\League\Classes\League;
use JetBrains\PhpStorm\Pure;
use App\Services\League\Classes\Team;

class CalculatePredictions
{
    public const MAX_PREDICTION = 75;

    public const MIN_PREDICTION = 25;

    private int $sumPTS;

    public function handle(League $league)
    {
        $teams = $league->getTeams();

        $this->sumPTS = $this->getSumPTS($teams);

        foreach ($teams as $team) {
            if ($team->getPlayed() > 0) {
                $team->setPrediction($this->getNewPrediction($team->getPTS()));
            }
        }
    }

    #[Pure] private function getSumPTS(array $teams): int
    {
        return array_reduce($teams, function (int $value, Team $team) {
            $value += $team->getPTS();

            return $value;
        }, 0);
    }

    private function getNewPrediction(int $oldPts): int
    {
        return max(
            min(round(($oldPts / $this->sumPTS) * 100), self::MAX_PREDICTION),
            self::MIN_PREDICTION);
    }
}
