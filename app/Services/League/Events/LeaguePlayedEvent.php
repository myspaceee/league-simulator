<?php

namespace App\Services\League\Events;

use App\Services\League\Classes\League;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use JetBrains\PhpStorm\Pure;

class LeaguePlayedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public array $teams;

    #[Pure] public function __construct(private League $league)
    {
        $this->teams = $this->league->getTeams();
    }
}
