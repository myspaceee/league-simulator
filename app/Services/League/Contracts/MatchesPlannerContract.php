<?php

namespace App\Services\League\Contracts;

use App\Services\League\Classes\Game;

interface MatchesPlannerContract
{
    /**
     * @return Game[]
     */
    public function plan() : array;
}
