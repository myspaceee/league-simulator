<?php

namespace App\Services\League\Contracts;

use App\Services\League\Classes\Game;

interface LeagueContract
{
    public function createAndSave(): string;

    /**
     * @return Game[]
     */
    public function playWeek(string $league_uuid);

    /**
     * @return Game[]
     */
    public function playAllWeeks(string $league_uuid);

    public function getLeagueResults(string $league_uuid): array;
}
