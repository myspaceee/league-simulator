<?php

namespace App\Services\League\Contracts;

use App\Services\League\Classes\Team;

interface TeamsBuilderContract
{
    /**
     * @return Team[]
     */
    public function build(int $teams_number): array;
}
