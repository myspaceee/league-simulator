<?php

namespace App\Services\League\Contracts;

interface PlayStrategyContract
{
    public function play(
        int $matches_per_week,
        int $current_week,
        array $matches
    ): array;
}
