<?php

namespace App\Services\League\Factories;

use App\Services\League\Classes\Game;

class GameFactory
{
    private CalculateGoalsFactory $calculateGoalsFactory;

    private GameTeamResultsFactory $gameTeamResultsFactory;

    public function __construct()
    {
        $this->calculateGoalsFactory = new CalculateGoalsFactory();
        $this->gameTeamResultsFactory = new GameTeamResultsFactory();
    }

    /**
     * @throws \App\Services\League\Exceptions\GameMembersException
     */
    public function build(array $teams): Game
    {
        return new Game(
            $teams,
            $this->calculateGoalsFactory,
            $this->gameTeamResultsFactory
        );
    }
}
