<?php

namespace App\Services\League\Factories;

use App\Services\League\Classes\League;
use App\Services\League\Classes\PlayStrategyResolver;
use App\Services\League\Classes\TeamsBuilder;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Filesystem\Filesystem;

class LeagueFactoryRestore
{
    private MatchesPlannerFactory $matchesPlannerFactory;

    private PlayStrategyResolver $playStrategyResolver;

    public function __construct(private Dispatcher $dispatcher)
    {
        $this->matchesPlannerFactory = new MatchesPlannerFactory();
        $this->playStrategyResolver = new PlayStrategyResolver();
    }

    public function restore(
        string $uuid,
        $matches_per_week,
        array $teams,
        array $matches,
        int $current_week,
        array $last_played_matches
    ): League
    {
        return new League(
            $uuid,
            $teams,
            $this->playStrategyResolver,
            $this->matchesPlannerFactory,
            $this->dispatcher,
            $matches_per_week,
            $matches,
            $current_week,
            $last_played_matches
        );
    }
}
