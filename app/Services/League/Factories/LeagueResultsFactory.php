<?php

namespace App\Services\League\Factories;

use App\Services\League\Classes\League;
use App\Services\League\Responses\LeagueResults;

class LeagueResultsFactory
{
    public function build(League $league): LeagueResults
    {
        return (new LeagueResults($league));
    }
}
