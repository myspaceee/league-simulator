<?php

namespace App\Services\League\Factories;

use App\Services\League\Classes\MatchesPlanner;

class MatchesPlannerFactory
{
    private GameFactory $gameFactory;

    public function __construct()
    {
        $this->gameFactory = new GameFactory();
    }

    public function build(array $teams): array
    {
        return (new MatchesPlanner($teams, $this->gameFactory))->plan();
    }
}
