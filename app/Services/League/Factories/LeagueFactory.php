<?php

namespace App\Services\League\Factories;

use App\Services\League\Classes\League;
use App\Services\League\Classes\PlayStrategyResolver;
use App\Services\League\Classes\TeamsBuilder;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Filesystem\Filesystem;

class LeagueFactory
{
    private TeamsBuilder $teamsBuilder;

    private MatchesPlannerFactory $matchesPlannerFactory;

    private PlayStrategyResolver $playStrategyResolver;

    public function __construct(private Dispatcher $dispatcher, private Filesystem $storage)
    {
        $this->teamsBuilder = new TeamsBuilder($this->storage);
        $this->matchesPlannerFactory = new MatchesPlannerFactory();
        $this->playStrategyResolver = new PlayStrategyResolver();
    }

    public function build($matches_per_week, $teams_number): League
    {
        $teams = $this->teamsBuilder->build($teams_number);

        $uuid = uniqid();

        return new League(
            $uuid,
            $teams,
            $this->playStrategyResolver,
            $this->matchesPlannerFactory,
            $this->dispatcher,
            $matches_per_week
        );
    }
}
