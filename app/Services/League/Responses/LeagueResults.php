<?php

namespace App\Services\League\Responses;

use JetBrains\PhpStorm\ArrayShape;
use App\Services\League\Classes\League;
use App\Services\League\Classes\Team;

class LeagueResults
{
    public function __construct(private League $league)
    {
    }

    #[ArrayShape(['current_week' => "int", 'teams' => "array", 'last_played_matches' => "array"])]
    public function format(): array
    {
        return [
            'current_week' => $this->league->getCurrentWeek(),
            'teams' => $this->formatTeams(),
            'last_played_matches' => $this->league->getLastPlayedMatches()
        ];
    }

    private function formatTeams(): array
    {
        return array_map(function (Team $team) {
            return [
                'name' => $team->getName(),
                'pts' => $team->getPTS(),
                'played' => $team->getPlayed(),
                'won' => $team->getWon(),
                'drawn' => $team->getDrawn(),
                'lost' => $team->getLost(),
                'gd' => $team->getGD(),
                'prediction_score' => $team->getPrediction()
            ];
        }, $this->league->getTeams());
    }
}
