<?php

namespace App\Providers;

use App\Services\League\Classes\LeagueInterface;
use App\Services\League\Contracts\LeagueContract;
use App\Services\League\Facades\League;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('league', LeagueInterface::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
