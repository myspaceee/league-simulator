<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeagueStartRequest;
use App\Services\League\Facades\League;
use Illuminate\Http\Request;

class LeagueController extends Controller
{
    //
    public function create(LeagueStartRequest $request)
    {
        $uuid = League::createAndSave(
            $request->input('games_number_per_week', 2),
            $request->input('teams_number', 4)
        );

        return route('league.show', [
            'leagueUUID' => $uuid
        ]);
    }

    public function show(string $leagueUUID)
    {
        return League::getLeagueResults($leagueUUID);
    }

    public function playNextWeek(string $leagueUUID)
    {
        League::playWeek($leagueUUID);
    }

    public function playAllWeeks(string $leagueUUID)
    {
        League::playAllWeeks($leagueUUID);
    }
}
